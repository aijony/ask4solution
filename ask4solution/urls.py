# ask4solution/urls.py

from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user/', include('users.urls')),
    path('user/', include('django.contrib.auth.urls')),
    path('', include('a4s.urls')),
]
