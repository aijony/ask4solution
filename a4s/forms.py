# a4s/forms.py

from django import forms
from django.contrib.auth import get_user_model

from .models import Question, Answer


class QuestionForm(forms.ModelForm):
    asker = forms.ModelChoiceField(
        widget=forms.HiddenInput,
        queryset=get_user_model().objects.all(),
        disabled=True,  # ignore any values provided by data (from a request)
    )
    description = forms.CharField(
        widget=forms.Textarea(
            attrs={'rows': 7, 'cols': 40}
        )
    )

    class Meta:
        model = Question
        fields = ['title', 'description', 'asker', ]


class AnswerForm(forms.ModelForm):
    solver = forms.ModelChoiceField(
        widget=forms.HiddenInput,
        queryset=get_user_model().objects.all(),
        disabled=True,  # ignore any values provided by data (from a request)
    )
    question = forms.ModelChoiceField(
        widget=forms.HiddenInput,
        queryset=Question.objects.all(),
        disabled=True,  # ignore any values provided by data (from a request)
    )
    description = forms.CharField(
        widget=forms.Textarea(
            attrs={'rows': 5, 'cols': 40}
        )
    )

    class Meta:
        model = Answer
        fields = ['description', 'solver', 'question']


class AnswerAcceptanceForm(forms.ModelForm):
    accepted = forms.BooleanField(
        widget=forms.HiddenInput,
        required=False,
    )

    class Meta:
        model = Answer
        fields = ['accepted', ]
