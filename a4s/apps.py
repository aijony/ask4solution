from django.apps import AppConfig


class A4SConfig(AppConfig):
    name = 'a4s'
