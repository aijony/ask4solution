# a4s/views.py

from django.views.generic import (
    TemplateView,
    DetailView,
    ListView,
    CreateView,
    UpdateView,
    DayArchiveView,
    RedirectView,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponseRedirect, HttpResponseBadRequest
from django.utils.text import slugify
from django.urls.base import reverse
from django.utils import timezone

from .models import Question, Answer
from .forms import QuestionForm, AnswerForm, AnswerAcceptanceForm


class HomeView(TemplateView):
    template_name = 'a4s/home.html'


class AskQuestionView(LoginRequiredMixin, CreateView):
    form_class = QuestionForm
    template_name = 'a4s/ask_question.html'

    # initial values provided to the form
    def get_initial(self):
        return {
            'asker': self.request.user.id
        }

    def form_valid(self, form):
        action = self.request.POST.get('action')
        if action == 'SAVE':
            form.instance.slug = slugify(form.cleaned_data['title'])
            # save and redirect
            return super().form_valid(form)
        elif action == 'PREVIEW':
            preview = Question(
                title=form.cleaned_data['title'],
                description=form.cleaned_data['description'],
            )
            ctx = self.get_context_data(preview=preview)
            return self.render_to_response(context=ctx)
        return HttpResponseBadRequest()


class QuestionDetailView(DetailView):
    model = Question
    template_name = 'a4s/question_detail.html'

    ACCEPT_FORM = AnswerAcceptanceForm(initial={'accepted': True})
    REJECT_FORM = AnswerAcceptanceForm(initial={'accepted': False})

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({
            'answer_form': AnswerForm(initial={
                'solver': self.request.user.id,
                'question': self.object.id,
            })
        })
        if self.object.can_accept_answers(self.request.user):
            ctx.update({
                'accept_form': self.ACCEPT_FORM,
                'reject_form': self.REJECT_FORM,
            })
        return ctx


class CreateAnswerView(LoginRequiredMixin, CreateView):
    form_class = AnswerForm
    template_name = 'a4s/create_answer.html'

    def get_initial(self):
        return {
            'question': self.get_question().id,
            'solver': self.request.user.id,
        }

    def get_context_data(self, **kwargs):
        return super().get_context_data(question=self.get_question(), **kwargs)

    def get_success_url(self):
        return self.object.question.get_absolute_url()

    def form_valid(self, form):
        action = self.request.POST.get('action')
        if action == 'SAVE':
            # save and redirect as usual.
            return super().form_valid(form)
        elif action == 'PREVIEW':
            ctx = self.get_context_data(
                preview=form.cleaned_data['description'])
            return self.render_to_response(context=ctx)
        return HttpResponseBadRequest()

    def get_question(self):
        return Question.objects.get(pk=self.kwargs['pk'])


class UpdateAnswerAcceptanceView(LoginRequiredMixin, UpdateView):
    form_class = AnswerAcceptanceForm
    queryset = Answer.objects.all()

    def get_success_url(self):
        return self.object.question.get_absolute_url()

    def form_invalid(self, form):
        return HttpResponseRedirect(
            redirect_to=self.object.question.get_absolute_url())


class DailyQuestionList(DayArchiveView):
    template_name = 'a4s/question_archive_day.html'
    queryset = Question.objects.all()
    date_field = 'created'
    month_format = '%m'
    allow_empty = True


class TodaysQuestionList(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        today = timezone.now()
        return reverse(
            'a4s:daily_question_list',
            kwargs={
                'day': today.day,
                'month': today.month,
                'year': today.year,
            }
        )


class QuestionListView(ListView):
    model = Question
    template_name = 'a4s:question_list'
    paginate_by = 3
