# a4s/urls.py

from django.urls import path

from .views import (
    HomeView,
    AskQuestionView,
    QuestionDetailView,
    CreateAnswerView,
    UpdateAnswerAcceptanceView,
    DailyQuestionList,
    TodaysQuestionList,
    QuestionListView,
)


app_name = 'a4s'
urlpatterns = [
    path('',
         HomeView.as_view(),
         name='home'
         ),
    path('ask/',
         AskQuestionView.as_view(),
         name='ask_question'
         ),
    path('question/<int:year>/<int:month>/<int:day>/<slug:slug>/',
         QuestionDetailView.as_view(),
         name='question_detail'
         ),
    path('question/<int:pk>/answer/',
         CreateAnswerView.as_view(),
         name='create_answer'
         ),
    path('answer/<int:pk>/accept/',
         UpdateAnswerAcceptanceView.as_view(),
         name='update_answer_acceptance'
         ),
    path('questions/<int:year>/<int:month>/<int:day>/',
         DailyQuestionList.as_view(),
         name='daily_question_list'
         ),
    path('questions/today/',
         TodaysQuestionList.as_view(),
         name='daily_question_list'
         ),
    path('questions/',
         QuestionListView.as_view(),
         name='question_list'
         ),
]
