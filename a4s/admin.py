from django.contrib import admin

from .models import Question, Answer


# @admin.register() decorator performs the same function
# as the admin.site.register()
@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'asker')
    list_editable = ('description',)
    prepopulated_fields = {'slug': ('title',)}
    ordering = ('created',)


admin.site.register(Answer)
