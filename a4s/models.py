# a4s/models.py

from django.db import models
from django.urls import reverse
from django.contrib.auth import get_user_model


class Question(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField()
    asker = models.ForeignKey(to=get_user_model(), on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=250, unique_for_date='created')

    def __str__(self):
        return self.title

    # def get_absolute_url(self):
    #     return reverse("a4s:question_detail", kwargs={"pk": self.pk})

    # for generating seo freindly url by using slug
    def get_absolute_url(self):
        return reverse(
            'a4s:question_detail',
            args=[
                self.created.year,
                self.created.month,
                self.created.day,
                self.slug
            ]
        )

    def can_accept_answers(self, user):
        return user == self.asker


class Answer(models.Model):
    description = models.TextField()
    solver = models.ForeignKey(to=get_user_model(), on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    question = models.ForeignKey(to=Question, on_delete=models.CASCADE)
    accepted = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)
